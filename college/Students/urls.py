from django.conf.urls import url
from Students.views import StudentsView,DepartmentsView
from django.views.decorators.csrf import csrf_exempt
urlpatterns = [
   url(r'^students/get_details/$',csrf_exempt(StudentsView.as_view())),
   url(r'^departments/get_details/$',csrf_exempt(DepartmentsView.as_view())),
]
