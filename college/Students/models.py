# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

#Department Model (IT,ECE,CS)
class Departments(models.Model):
    department_name=models.CharField(max_length=255,null=False,unique=True)
    department_head=models.CharField(max_length=255,null=True,blank=True)
    department_id=models.CharField(max_length=255,unique=True,primary_key=True)
    def __unicode__(self):
        return str(self.department_id)+ ' ' +str(self.department_name)+ ' ' +str(self.department_head)

# Students model
class Students(models.Model):
    first_name=models.CharField(max_length=255,null=False)
    last_name=models.CharField(max_length=255,null=True,blank=True)
    college_id=models.CharField(max_length=255,unique=True,primary_key=True)
    department_id=models.ForeignKey(Departments)
    def __unicode__(self):
        return str(self.college_id)+ ' ' +str(self.first_name)+ ' ' +str(self.last_name)


