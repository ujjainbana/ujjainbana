# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import View
from django.http import HttpResponse, QueryDict, Http404, HttpResponseNotFound
import json
from Students.models import Students, Departments 
from django.core.exceptions import ObjectDoesNotExist

#for getting info about students in the college
class StudentsView(View):

    #get: Fetch the details of the students
    def get(self,request,*args,**kwargs):
        _params = request.GET #_params ={college_id,first_name,last_name}
        details=[]
        students=Students.objects.all()
        if students.count() > 0: #len(student)
            for i in students:
                details.append({
                'college_id':i.college_id,
                'first_name': i.first_name,
                'last_name': i.last_name,
                })
            return HttpResponse(json.dumps(details), content_type='application/json')
        
        else:
            return HttpResponseNotFound ("There is no Student in the college of this college_id: "+_params.get('college_id'))
    
    #post: Add students
    def post(self,request,*args,**kwargs):
        data=request.POST
        dep_exist=Departments.objects.filter(department_id=data.get('department_id'))
        if dep_exist.count()>0:
            add_students=Students(college_id=data.get('college_id'),first_name=data.get('first_name'),last_name=data.get('last_name'),department_id_id=data.get('department_id'))
            check_length=Students.objects.is_valid(data.get('college_id'),data.get('first_name'),data.get('last_name'))
            if check_length:
                add_students.save()
                return HttpResponse(data.get('first_name') + " added successfully")
            else:
                return HttpResponseNotFound("lenth of all the fields should not be greater than 255")
        else:
            return HttpResponseNotFound("department_id: "+data.get('department_id')+" doesn't exist")
    
    #put: update info of the students
    def put(self,request,*args,**kwargs):
        data=QueryDict(request.body) 
        try:
            update_object=Students.objects.get(college_id=data.get('college_id'))
            update_object.first_name=data.get('first_name')
            check_length=Students.objects.is_valid(data.get('college_id'),data.get('first_name'))  
            if check_length:
                update_object.save()
                return HttpResponse(data.get('college_id') + "'s First name updated")
            else:
                return HttpResponseNotFound("lenth of all the fields should not be greater than 255")      
        except ObjectDoesNotExist:
            return HttpResponseNotFound("College id :"+data.get('college_id')+" doesn't exist")
    
    #To remove student info from college
    def delete(self,request,*args,**kwargs):
        data=QueryDict(request.body)
        try:
            student=Students.objects.get(college_id=data.get('college_id')) 
            student.delete()
            return HttpResponse(data.get('college_id')+" deleted successfully")
        
        except ObjectDoesNotExist:
            return HttpResponseNotFound(data.get('college_id')+" doesn't Exist")

class DepartmentsView(View):
    
    #get: Fetch the details of the students as well as department
    def get(self,request,*args,**kwargs):
        _params = request.GET #_params ={student's college_id}
        student=Students.objects.get(college_id=_params.get('college_id'))
        department_details=({
        'college_id':student.college_id,
        'first_name':student.first_name,
        'last_name':student.last_name,
        'department_name':student.department_id.department_name,
        })
        return HttpResponse(json.dumps(department_details), content_type='application/json')
    
    #post: Add department
    def post(self,request,*args,**kwargs):
        data=request.POST
        add_department=Departments(department_id=data.get('department_id'),department_name=data.get('department_name'),department_head=data.get('department_head'))
        check_length=Departments.objects.is_valid(data.get('department_id'),data.get('department_name'),data.get('department_head'))
        if check_length:
            add_department.save()
            return HttpResponse(data.get('department_name') + " added successfully")
        else:
            return HttpResponseNotFound("Length of all the fields should not exceed 255")

    #put: update department of the students
    def put(self,request,*args,**kwargs):
        data=QueryDict(request.body)
        try:
            update_department=Students.objects.get(college_id=data.get('college_id')) 
            update_department.department_id_id=data.get('department_id')
            check_length=Students.objects.is_valid(data.get('college_id'),data.get('department_id'))
            if check_length:
                update_department.save()
                department_object=Departments.objects.get(department_id=data.get('department_id'))
                return HttpResponse(data.get('college_id') + "'s department updated to " + department_object.department_name)
            else:
                return HttpResponseNotFound("lenth of all the fields should not be greater than 255")
        except ObjectDoesNotExist:
            return HttpResponseNotFound(data.get('college_id')+": doesn't exist")