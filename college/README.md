1. APi- "College"
   This api is simply use for adding, deleting, updating or fetching the details of students and corresponding departments (ex. IT,ECE,Mech,CSE) They are studying.
2. This Api has one app ("Students")
   In that app we have two modules (students and departments)- Both are connected to each other by foreign key (department_id)
3. Student_Module (College_id(Pk), first_name, last_name, department_id(fk))
4. Department_Module (department_id(pk), department_name, department_head)
5. I have created two different class (inside view class of app) for getting details(Add,update,delete,fetch) of student and department module
6. Get function(Students)
   6.1 To fetch the details of the student
   6.2 if student table is empty it will show custom exception("No students in the college exist of perticular college_id")
7. Post function (Students)
   7.1 To add data in the table student (foreign key(department_id_id) cant be null)
8. Put Function(Students)
   8.1 To update the details  of the students (ex. first_name,last_name)
9. Delete Function(Students)
   9.1 To remove any student from the college database
10. Get function(departments)
   10.1 To fetch the details of the department according to college_id of the student
   10.2 if student table is empty it will show custom exception("No students in the college exist of perticular college_id")
11. Post function (Departments)
   11.1 To add departments in the college
12. Put Function(Departments)
   12.1 To update the department of the students
   12.2 If student doesnt exist in the college then it will show custom error("Doesnt exist")
